package org.sikong.ifc.ifc2x3.core.extension.product.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * The IfcCoveringTypeEnum defines the range of different types of covering
 * that can be specified.
 * @since IFC Release 1.0
 */
object IfcCoveringTypeEnum extends IfcEnumeration {
  type IfcCoveringTypeEnum = Value

  // the covering is used to represent a ceiling
  val CEILING: Value = Value("CEILING")
  // the covering is used to represent a flooring
  val FLOORING: Value = Value("FLOORING")
  //  the covering is used to represent a cladding
  val CLADDING: Value = Value("CLADDING")
  // the covering is used to represent a roof
  val ROOFING: Value = Value("ROOFING")
  // the covering is used to insulate an element for thermal or acoustic purposes.
  val INSULATION: Value = Value("INSULATION")
  // an impervious layer that could be used for e.g. roof covering (below tiling -
  // that may be known as sarking etc.) or as a damp proof course membrane
  val MEMBRANE: Value = Value("MEMBRANE")
  // the covering is used to isolate a distribution element from a space in which
  // it is contained.
  val SLEEVING: Value = Value("SLEEVING")
  // the covering is used for wrapping particularly of distribution elements using tape.
  val WRAPPING: Value = Value("WRAPPING")
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
