package org.sikong.ifc.ifc2x3.core.extension.process.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * An IfcWorkControlTypeEnum is an enumeration data type that specifies
 * the types of work control from which the relevant control can be selected.
 * @since IFC Release 2.0
 */
object IfcWorkControlTypeEnum extends IfcEnumeration {
  type IfcWorkControlTypeEnum = Value

  // A control in which actual items undertaken are indicated.
  val ACTUAL: Value = Value("ACTUAL")
  // A control that is a baseline from which changes that are made later can be recognized.
  val BASELINE: Value = Value("BASELINE")
  // A control showing planned items.
  val PLANNED: Value = Value("PLANNED")
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
