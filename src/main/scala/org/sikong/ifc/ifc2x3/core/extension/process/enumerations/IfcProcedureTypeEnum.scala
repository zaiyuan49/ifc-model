package org.sikong.ifc.ifc2x3.core.extension.process.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * The IfcProcedureTypeEnum defines the range of different types of
 * procedure that can be specified.
 * @since IFC 2x2
 */
object IfcProcedureTypeEnum extends IfcEnumeration {
  type IfcProcedureTypeEnum = Value

  // A caution that should be taken note of as a procedure or when
  // carrying out a procedure.
  val ADVICE_CAUTION: Value = Value("ADVICE_CAUTION")
  // Additional information or advice that should be taken note of as
  // a procedure or when carrying out a procedure.
  val ADVICE_NOTE: Value = Value("ADVICE_NOTE")
  // A warning of potential danger that should be taken note of as a
  // procedure or when carrying out a procedure.
  val ADVICE_WARNING: Value = Value("ADVICE_WARNING")
  // A procedure undertaken to calibrate an artefact.
  val CALIBRATION: Value = Value("CALIBRATION")
  val DIAGNOSTIC: Value = Value("DIAGNOSTIC")
  // A procedure undertaken to shutdown the operation an artefact.
  val SHUTDOWN: Value = Value("SHUTDOWN")
  // A procedure undertaken to start up the operation an artefact.
  val STARTUP: Value = Value("STARTUP")
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
