package org.sikong.ifc.ifc2x3.core.extension.product.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the available generic types for IfcSpaceType.
 * @since Release IFC2x Edition 3.
 */
object IfcSpaceTypeEnum extends IfcEnumeration {
  type IfcSpaceTypeEnum = Value

  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
