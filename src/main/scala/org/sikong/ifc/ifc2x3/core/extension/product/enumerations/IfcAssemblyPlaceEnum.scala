package org.sikong.ifc.ifc2x3.core.extension.product.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * Enumeration defining where the assembly is intended to take place,
 * either in a factory or on the building site.
 * @since Release IFC2x Edition 2
 */
object IfcAssemblyPlaceEnum extends IfcEnumeration {
  type IfcAssemblyPlaceEnum = Value

  val SITE: Value = Value("SITE")
  val FACTORY: Value = Value("FACTORY")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
