package org.sikong.ifc.ifc2x3.core.extension.control.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * Defines the type of time series schedule, such as daily, weekly, monthly or annually.
 * @since Release IFC2x Edition 2
 */
object IfcTimeSeriesScheduleTypeEnum extends IfcEnumeration {
  type IfcTimeSeriesScheduleTypeEnum = Value

  // An annual time series schedule.
  val ANNUAL: Value = Value("ANNUAL")
  // A monthly time series schedule.
  val MONTHLY: Value = Value("MONTHLY")
  // A weekly time series schedule.
  val WEEKLY: Value = Value("WEEKLY")
  // A daily time series schedule.
  val DAILY: Value = Value("DAILY")
  // A user defined time series schedule type to be provided.
  val USERDEFINED: Value = Value("USERDEFINED")
  // Time series schedule type not defined.
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
