package org.sikong.ifc.ifc2x3.core.kernal.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the applicable object categories (i.e. the subtypes
 * at the 2nd level of the IFC inheritance tree) . Attached to an object, it
 * indicates to which subtype of IfcObject the entity referencing it would
 * otherwise comply with.
 * @since IFC Release 1.0
 */
object IfcObjectTypeEnum extends IfcEnumeration {
  type IfcObjectTypeEnum = Value

  val PRODUCT: Value = Value("PRODUCT")
  val PROCESS: Value = Value("PROCESS")
  val CONTROL: Value = Value("CONTROL")
  val RESOURCE: Value = Value("RESOURCE")
  val ACTOR: Value = Value("ACTOR")
  val GROUP: Value = Value("GROUP")
  val PROJECT: Value = Value("PROJECT")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
