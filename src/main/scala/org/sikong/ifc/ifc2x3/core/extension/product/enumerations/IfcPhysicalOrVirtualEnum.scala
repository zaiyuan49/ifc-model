package org.sikong.ifc.ifc2x3.core.extension.product.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the different types of space boundaries in terms
 * of its physical manifestation. A space boundary can either be physically
 * dividing or can be a virtual divider.
 * @since IFC Release 2.0
 */
object IfcPhysicalOrVirtualEnum extends IfcEnumeration {
  type IfcPhysicalOrVirtualEnum = Value

  // The space boundary is provided physically, i.e. by an physical element.
  val PHYSICAL: Value = Value("PHYSICAL")
  // The space boundary is provided virtually, i.e. by a logical divider that
  // has no physical manifestation.
  val VIRTUAL: Value = Value("VIRTUAL")
  // No information available.
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
