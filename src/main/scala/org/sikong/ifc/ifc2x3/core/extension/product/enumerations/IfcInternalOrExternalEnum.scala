package org.sikong.ifc.ifc2x3.core.extension.product.enumerations

/**
 * This enumeration defines the different types of spaces or space
 * boundaries in terms of either being inside the building or outside
 * the building.
 * @since IFC Release 2.0
 */
object IfcInternalOrExternalEnum extends Enumeration {
  type IfcInternalOrExternalEnum = Value

  /**
   * IfcSpace	The space is an internal space, fully enclosed by physical
   *          boundaries (directly or indirectly through adjacent spaces).
   * IfcSpaceBoundary	The space boundary faces to the inside of an internal
   *          space.
   */
  val INTERNAL = Value("INTERNAL")
  /**
   * IfcSpace	The space is an external space, not (or only partially)
   *          enclosed by physical boundaries.
   * IfcSpaceBoundary	The space boundary faces to the outer space, or the
   *          inside of an external space.
   */
  val EXTERNAL = Value("EXTERNAL")
  // No information available.
  val NOTDEFINED = Value("NOTDEFINED")
}
