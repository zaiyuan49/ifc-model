package org.sikong.ifc.ifc2x3.core.extension.product.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration is used to identify primary transport element types.
 * @since IFC R2x
 */
object IfcTransportElementTypeEnum extends IfcEnumeration {
  type IfcTransportElementTypeEnum = Value

  val ELEVATOR: Value = Value("ELEVATOR")
  val ESCALATOR: Value = Value("ESCALATOR")
  val MOVINGWALKWAY: Value = Value("MOVINGWALKWAY")
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
