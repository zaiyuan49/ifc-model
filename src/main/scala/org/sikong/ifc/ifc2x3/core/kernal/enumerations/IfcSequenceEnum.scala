package org.sikong.ifc.ifc2x3.core.kernal.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the different ways, in which a time lag
 * is applied to a sequence between two processes.
 * @since IFC Release 1.0
 */
object IfcSequenceEnum extends IfcEnumeration {
  type IfcSequenceEnum = Value

  val START_START: Value = Value("START_START")
  val START_FINISH: Value = Value("START_FINISH")
  val FINISH_START: Value = Value("FINISH_START")
  val FINISH_FINISH: Value = Value("FINISH_FINISH")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
