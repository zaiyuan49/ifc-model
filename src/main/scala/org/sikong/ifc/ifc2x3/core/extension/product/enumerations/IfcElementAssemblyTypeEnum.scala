package org.sikong.ifc.ifc2x3.core.extension.product.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * An enumeration defining the basic configuration types for element assemblies.
 * @since Release IFC2x Edition 2
 */
object IfcElementAssemblyTypeEnum extends IfcEnumeration {
  type IfcElementAssemblyTypeEnum = Value

  val ACCESSORY_ASSEMBLY: Value = Value("ACCESSORY_ASSEMBLY")
  val ARCH: Value = Value("ARCH")
  val BEAM_GRID: Value = Value("BEAM_GRID")
  val BRACED_FRAME: Value = Value("BRACED_FRAME")
  val GIRDER: Value = Value("GIRDER")
  val REINFORCEMENT_UNIT: Value = Value("REINFORCEMENT_UNIT")
  val RIGID_FRAME: Value = Value("RIGID_FRAME")
  val SLAB_FIELD: Value = Value("SLAB_FIELD")
  val TRUSS: Value = Value("TRUSS")
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
