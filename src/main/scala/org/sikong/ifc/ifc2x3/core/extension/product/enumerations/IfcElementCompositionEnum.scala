package org.sikong.ifc.ifc2x3.core.extension.product.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 *  Enumeration that provides an indication, whether the spatial
 *  structure element or proxy
 *  @since IFC Release 2.x<
 */
object IfcElementCompositionEnum extends IfcEnumeration {
  type IfcElementCompositionEnum = Value

  // a group or aggregation of similar elements
  val COMPLEX: Value = Value("COMPLEX")
  // a (undivided) element itself
  val ELEMENT: Value = Value("ELEMENT")
  //  a subelement or part
  val PARTIAL: Value = Value("PARTIAL")

}
