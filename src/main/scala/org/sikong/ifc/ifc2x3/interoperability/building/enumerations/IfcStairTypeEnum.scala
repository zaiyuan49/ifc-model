package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

object IfcStairTypeEnum extends IfcEnumeration {
  type IfcStairTypeEnum = Value

  // A stair extending from one level to another without turns or winders. The
  // stair consists of one straight flight.
  val STRAIGHT_RUN_STAIR: Value = Value("STRAIGHT_RUN_STAIR")
  // A straight stair consisting of two straight flights without turns but with one landing.
  val TWO_STRAIGHT_RUN_STAIR: Value = Value("TWO_STRAIGHT_RUN_STAIR")
  // A stair consisting of one flight with a quarter winder, which is making a 90° turn. The
  // direction of the turn is determined by the walking line.
  val QUARTER_WINDING_STAIR: Value = Value("QUARTER_WINDING_STAIR")
  // A stair making a 90° turn, consisting of two straight flights connected by a quarterspace
  // landing. The direction of the turn is determined by the walking line.
  val QUARTER_TURN_STAIR: Value = Value("QUARTER_TURN_STAIR")
  // A stair consisting of one flight with one half winder, which makes a 180° turn. The orientation
  // of the turn is determined by the walking line.
  val HALF_WINDING_STAIR: Value = Value("HALF_WINDING_STAIR")
  // A stair making a 180° turn, consisting of two straight flights connected by a halfspace landing.
  // The orientation of the turn is determined by the walking line.
  val HALF_TURN_STAIR: Value = Value("HALF_TURN_STAIR")
  // 	A stair consisting of one flight with two quarter winders, which make a 90° turn. The stair makes a
  // 	180° turn. The direction of the turns is determined by the walking line.
  val TWO_QUARTER_WINDING_STAIR: Value = Value("TWO_QUARTER_WINDING_STAIR")
  // 	A stair making a 180° turn, consisting of three straight flights connected by two quarterspace landings.
  // 	The direction of the turns is determined by the walking line.
  val TWO_QUARTER_TURN_STAIR: Value = Value("TWO_QUARTER_TURN_STAIR")
  // 	A stair consisting of one flight with three quarter winders, which make a 90° turn. The stair makes a 270°
  // 	turn. The direction of the turns is determined by the walking line.
  val THREE_QUARTER_WINDING_STAIR: Value = Value("THREE_QUARTER_WINDING_STAIR")
  // 	A stair making a 270° turn, consisting of four straight flights connected by three quarterspace landings.
  // 	The direction of the turns is determined by the walking line.
  val THREE_QUARTER_TURN_STAIR: Value = Value("THREE_QUARTER_TURN_STAIR")
  // A stair constructed with winders around a circular newel often without landings. Depending on outer boundary
  // it can be either a circular, elliptical or rectancular spiral stair. The orientation of the winding stairs is
  // determined by the walking line.
  val SPIRAL_STAIR: Value = Value("SPIRAL_STAIR")
  // A stair having one straight flight to a wide quarterspace landing, and two side flights from that landing into
  // opposite directions. The stair is making a 90° turn. The direction of traffic is determined by the walking line.
  val DOUBLE_RETURN_STAIR: Value = Value("DOUBLE_RETURN_STAIR")
  // A stair extending from one level to another without turns or winders. The stair is consisting of one curved flight.
  val CURVED_RUN_STAIR: Value = Value("CURVED_RUN_STAIR")
  // A curved stair consisting of two curved flights without turns but with one landing.
  val TWO_CURVED_RUN_STAIR: Value = Value("TWO_CURVED_RUN_STAIR")
  // 	Free form stair (user defined operation type)
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
