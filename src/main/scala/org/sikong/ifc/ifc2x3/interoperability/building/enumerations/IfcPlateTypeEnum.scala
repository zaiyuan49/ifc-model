package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the different types of planar elements
 * @since Release IFC2x Edition 2
 */
object IfcPlateTypeEnum extends IfcEnumeration {
  type IfcPlateTypeEnum = Value

  // A planar element within a curtain wall, often consisting of a frame with fixed glazing.
  val CURTAIN_PANEL: Value = Value("CURTAIN_PANEL")
  // A planar, flat and thin element, comes usually as metal sheet, and is often used as an additonal part within an assembly.
  val SHEET: Value = Value("SHEET")
  // User-defined linear element.
  val USERDEFINED: Value = Value("USERDEFINED")
  // Undefined linear element
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
