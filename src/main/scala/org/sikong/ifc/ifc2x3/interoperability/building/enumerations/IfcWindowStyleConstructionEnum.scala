package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the basic types of construction of windows. The construction
 * type relates to the main material (or material combination) used for making the window.
 *
 * @since IFC Release 2.x
 */
object IfcWindowStyleConstructionEnum extends IfcEnumeration {
  type IfcWindowStyleConstructionEnum = Value

  val ALUMINIUM: Value = Value("ALUMINIUM")
  val HIGH_GRADE_STEEL: Value = Value("HIGH_GRADE_STEEL")
  val STEEL: Value = Value("STEEL")
  val WOOD: Value = Value("WOOD")
  val ALUMINIUM_WOOD: Value = Value("ALUMINIUM_WOOD")
  val PLASTIC: Value = Value("PLASTIC")
  val OTHER_CONSTRUCTION: Value = Value("OTHER_CONSTRUCTION")
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
