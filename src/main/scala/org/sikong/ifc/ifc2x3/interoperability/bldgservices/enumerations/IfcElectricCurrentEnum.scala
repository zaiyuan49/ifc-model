package org.sikong.ifc.ifc2x3.interoperability.bldgservices.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the different types of available electrical current
 *
 * @since IFC R2.0
 */
object IfcElectricCurrentEnum extends IfcEnumeration {
  type IfcElectricCurrentEnum = Value

  // Alternating current (AC)
  val ALTERNATING: Value = Value("ALTERNATING")
  // Direct current (DC)
  val DIRECT: Value = Value("DIRECT")
  // Undefined electrical current
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
