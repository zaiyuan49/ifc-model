package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the different ways how path based elements
 * (here IfcWallStandardCase) can connect..
 * @since IFC Release 2.0
 */
object IfcConnectionTypeEnum extends IfcEnumeration {
  type IfcConnectionTypeEnum = Value

  val ATPATH: Value = Value("ATPATH")
  val ATSTART: Value = Value("ATSTART")
  val ATEND: Value = Value("ATEND")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
