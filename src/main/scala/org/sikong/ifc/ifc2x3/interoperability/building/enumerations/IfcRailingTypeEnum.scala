package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * Enumeration defining the valid types of railings that can be predefined using
 * the enumeration values.
 * @since IFC Release 2.0
 */
object IfcRailingTypeEnum extends IfcEnumeration{
  type IfcRailingTypeEnum = Value

  // A type of railing designed to serve as an optional structural support for loads
  // applied by human occupants (at hand height). Generally located adjacent to ramps
  // and stairs. Generally floor or wall mounted.
  val HANDRAIL: Value = Value("HANDRAIL")
  // A type of railing designed to guard human occupants from falling off a stair,
  // ramp or landing where there is a vertical drop at the edge of such floors/landings.
  val GUARDRAIL: Value = Value("GUARDRAIL")
  // Similar to the definitions of a guardrail except the location is at the edge of
  // a floor, rather then a stair or ramp. Examples are balustrates at roof-tops or
  // balconies.
  val BALUSTRADE: Value = Value("BALUSTRADE")
  // User-defined railing element, a term to identify the user type is given by the
  // attribute IfcRailing.ObjectType.
  val USERDEFINED: Value = Value("USERDEFINED")
  // Undefined railing element, no type information available.
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
