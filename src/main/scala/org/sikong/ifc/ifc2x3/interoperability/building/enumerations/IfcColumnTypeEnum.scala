package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the different types of linear elements an
 * IfcColumnType object can fulfill:
 *    COLUMN, USERDEFINED, NOTDEFINED
 * @since Release IFC2x Edition 2
 */
object IfcColumnTypeEnum extends IfcEnumeration {
  type IfcColumnTypeEnum = Value

  // A standard column element usually used vertically.
  val COLUMN: Value = Value("COLUMN")
  // User-defined linear element.
  val USERDEFINED: Value = Value("USERDEFINED")
  // Undefined linear element
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
