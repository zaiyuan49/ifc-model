package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the different types of stair flights
 * @since Release IFC2x Edition 2
 */
object IfcStairFlightTypeEnum extends IfcEnumeration {
  type IfcStairFlightTypeEnum = Value

  // A stair flight with a straight walking line.
  val STRAIGHT: Value = Value("STRAIGHT")
  // A stair flight with a straight walking line.
  val WINDER: Value = Value("WINDER")
  // A stair flight with a circular or elliptic walking line.
  val SPIRAL: Value = Value("SPIRAL")
  // A stair flight with a curved walking line.
  val CURVED: Value = Value("CURVED")
  // A stair flight with a free form walking line (and outer boundaries).
  val FREEFORM: Value = Value("FREEFORM")
  // User-defined stair flight.
  val USERDEFINED: Value = Value("USERDEFINED")
  // Undefined stair flight.
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
