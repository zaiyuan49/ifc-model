package org.sikong.ifc.ifc2x3.interoperability.management.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * An IfcProjectOrderTypeEnum is a list of the types of project order that may be identified.
 *
 * @since IFC 2x2
 */
object IfcProjectOrderTypeEnum extends IfcEnumeration {
  type IfcProjectOrderTypeEnum = Value

  // An instruction to make a change to a product or work being undertaken and a
  // description of the work that is to be performed.
  val CHANGEORDER: Value = Value("CHANGEORDER")
  // An instruction to carry out maintenance work and a description of the work
  // that is to be performed.
  val MAINTENANCEWORKORDER: Value = Value("MAINTENANCEWORKORDER")
  // An instruction to move persons and artefacts and a description of the move
  // locations, objects to be moved, etc.
  val MOVEORDER: Value = Value("MOVEORDER")
  // An instruction to purchase goods and/or services and a description of the
  // goods and/or services to be purchased that is to be performed.
  val PURCHASEORDER: Value = Value("PURCHASEORDER")
  // A general instruction to carry out work and a description of the work to
  // be done. Note the difference between a work order generally and a maintenance
  // work order.
  val WORKORDER: Value = Value("WORKORDER")
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
