package org.sikong.ifc.ifc2x3.interoperability.bldgservices.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the different sound scales
 *
 * @since IFC R2.0
 */
object IfcSoundScaleEnum extends IfcEnumeration {
  type IfcSoundScaleEnum = Value

  // Decibels in an A-wieghted scale
  val DBA: Value = Value("DBA")
  // Decibels in an B-wieghted scale
  val DBB: Value = Value("DBB")
  // Decibels in an C-wieghted scale
  val DBC: Value = Value("DBC")
  // Noise criteria
  val NC: Value = Value("NC")
  // Noise rating
  val NR: Value = Value("NR")
  // User-defined sound scale
  val USERDEFINED: Value = Value("USERDEFINED")
  // Undefined sound scale
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
