package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the different types of linear elements
 * @since Release IFC2x Edition 2
 */
object IfcMemberTypeEnum extends IfcEnumeration{
  type IfcMemberTypeEnum = Value

  // A linear element (usually sloped) often used for bracing of a girder or truss.
  val BRACE: Value = Value("BRACE")
  // Upper or lower longitudinal member of a truss, used horizontally or sloped.
  val CHORD: Value = Value("CHORD")
  // A linear element (usually used horizontally) within a roof structure to connect rafters and posts.
  val COLLAR: Value = Value("COLLAR")
  // A linear element within a girder or truss with no further meaning.
  val MEMBER: Value = Value("MEMBER")
  // A linear element within a curtain wall system to connect two (or more) panels.
  val MULLION: Value = Value("MULLION")
  // A linear continuous horizontal element in wall framing, e.g. a head piece or a sole plate.
  val PLATE: Value = Value("PLATE")
  // A linear member (usually used vertically) within a roof structure to support purlins.
  val POST: Value = Value("POST")
  // A linear element (usually used horizontally) within a roof structure to support rafters
  val PURLIN: Value = Value("PURLIN")
  // A linear elements used to support roof slabs or roof covering, usually used with slope.
  val RAFTER: Value = Value("RAFTER")
  // A linear element used to support stair or ramp flights, usually used with slope.
  val STRINGER: Value = Value("STRINGER")
  // A linear element often used within a girder or truss.
  val STRUT: Value = Value("STRUT")
  // Vertical element in wall framing.
  val STUD: Value = Value("STUD")
  // User-defined linear element.
  val USERDEFINED: Value = Value("USERDEFINED")
  // Undefined linear element
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
