package org.sikong.ifc.ifc2x3.interoperability.bldgservices.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * The IfcDistributionChamberElementTypeEnum defines the range of different
 * types of distribution chamber that can be specified.
 *
 * @since IFC 2x2
 */
object IfcDistributionChamberElementTypeEnum extends IfcEnumeration {
  type IfcDistributionChamberElementTypeEnum = Value

  // Space formed in the ground for the passage of pipes, cables, ducts. (BS6100 100 3410)
  val FORMEDDUCT: Value = Value("FORMEDDUCT")
  // Chamber constructed on a drain, sewer or pipeline and with a removable cover, that
  // permits visble inspection..
  val INSPECTIONCHAMBER: Value = Value("INSPECTIONCHAMBER")
  // Recess or chamber formed to permit access for inspection of substructure and services.
  // (BS6100 221 4128 - modified)
  val INSPECTIONPIT: Value = Value("INSPECTIONPIT")
  // Chamber constructed on a drain, sewer or pipeline and with a removable cover, that
  // permits the entry of a person.
  val MANHOLE: Value = Value("MANHOLE")
  // Chamber that houses a meter(s) (BS6100 250 6224 - modified)
  val METERCHAMBER: Value = Value("METERCHAMBER")
  // Recess or small chamber into which liquid is drained to facilitate its removal
  // (BS6100 100 3431)
  val SUMP: Value = Value("SUMP")
  // Excavation, the length of which greatly exceeds the width. (BS6100 221 4118)
  val TRENCH: Value = Value("TRENCH")
  // 	Chamber that houses a valve(s) (BS6100 250 6224)
  val VALVECHAMBER: Value = Value("VALVECHAMBER")
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
