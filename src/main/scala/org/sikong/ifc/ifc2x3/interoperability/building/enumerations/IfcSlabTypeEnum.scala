package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the available predefined types of a slab. The
 * IfcSlabTypeEnum can be used for slab occurrences, IfcSlab, and slab types,
 * IfcSlabType. A special property set definition may be provided for each
 * predefined type.
 * @since IFC Release 2.0
 */
object IfcSlabTypeEnum extends IfcEnumeration {
  type IfcSlabTypeEnum = Value

  // The slab is used to represent a floor slab.
  val FLOOR: Value = Value("FLOOR")
  // The slab is used to represent a roof slab (either flat or sloped).
  val ROOF: Value = Value("ROOF")
  // 	The slab is used to represent a landing within a stair or ramp.
  val LANDING: Value = Value("LANDING")
  // 	The slab is used to represent a floor slab against the ground (and thereby
  // 	being a part of the foundation)
  val BASESLAB: Value = Value("BASESLAB")
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
