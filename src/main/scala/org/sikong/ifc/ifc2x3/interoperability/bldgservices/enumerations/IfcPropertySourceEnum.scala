package org.sikong.ifc.ifc2x3.interoperability.bldgservices.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration is used to qualify the life-cycle or design state of the
 * properties contained in the entity
 *
 * @since IFC 2x2
 */
object IfcPropertySourceEnum extends IfcEnumeration {
  type IfcPropertySourceEnum = Value

  // Properties are all design values.
  val DESIGN: Value = Value("DESIGN")
  // Properties are all maximum design values.
  val DESIGNMAXIMUM: Value = Value("DESIGNMAXIMUM")
  // Properties are all minimum design values.
  val DESIGNMINIMUM: Value = Value("DESIGNMINIMUM")
  // Property values are all the results of a simulation.
  val SIMULATED: Value = Value("SIMULATED")
  // Properties are all AS-BUILT values.
  val ASBUILT: Value = Value("ASBUILT")
  // Properties are all commissioning values.
  val COMMISSIONING: Value = Value("COMMISSIONING")
  // Property values are all the results of a measurement device, either automated or manual.
  val MEASURED: Value = Value("MEASURED")
  // Property values are defined by the user.
  val USERDEFINED: Value = Value("USERDEFINED")
  // Property values are not defined.
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
