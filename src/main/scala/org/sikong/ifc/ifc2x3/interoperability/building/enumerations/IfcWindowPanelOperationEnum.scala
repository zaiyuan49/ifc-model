package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

object IfcWindowPanelOperationEnum extends IfcEnumeration {
  type IfcWindowPanelOperationEnum = Value

  // panel that opens to the right when viewed from the outside
  val SIDEHUNGRIGHTHAND: Value = Value("SIDEHUNGRIGHTHAND")
  // panel that opens to the left when viewed from the outside
  val SIDEHUNGLEFTHAND: Value = Value("SIDEHUNGLEFTHAND")
  // panel that opens to the right and is bottom hung
  val TILTANDTURNRIGHTHAND: Value = Value("TILTANDTURNRIGHTHAND")
  // 	panel that opens to the left and is bottom hung
  val TILTANDTURNLEFTHAND: Value = Value("TILTANDTURNLEFTHAND")
  // panel is top hung
  val TOPHUNG: Value = Value("TOPHUNG")
  // panel is bottom hung
  val BOTTOMHUNG: Value = Value("BOTTOMHUNG")
  // 	panel is swinging horizontally (hinges are in the middle)
  val PIVOTHORIZONTAL: Value = Value("PIVOTHORIZONTAL")
  // 	panel is swinging vertically (hinges are in the middle)
  val PIVOTVERTICAL: Value = Value("PIVOTVERTICAL")
  // panel is sliding horizontally
  val SLIDINGHORIZONTAL: Value = Value("SLIDINGHORIZONTAL")
  // panel is sliding vertically
  val SLIDINGVERTICAL: Value = Value("SLIDINGVERTICAL")
  // panel is removable
  val REMOVABLECASEMENT: Value = Value("REMOVABLECASEMENT")
  // 	panel is fixed
  val FIXEDCASEMENT: Value = Value("FIXEDCASEMENT")
  // 	user defined operation type
  val OTHEROPERATION: Value = Value("OTHEROPERATION")
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
