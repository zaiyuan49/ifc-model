package org.sikong.ifc.ifc2x3.interoperability.bldgservices.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the flow direction at a connection point as either
 * a Source, Sink, or both SourceAndSink
 *
 * @since IFC R2.0
 */
object IfcFlowDirectionEnum extends IfcEnumeration {
  type IfcFlowDirectionEnum = Value

  // A source of flow (e.g., it flows out of the connection)
  val SOURCE: Value = Value("SOURCE")
  // A flow sink (e.g., it flows into the connection)
  val SINK: Value = Value("SINK")
  // Both a source and sink (e.g., it flows both into and out of the connection
  // simultaneously)
  val SOURCEANDSINK: Value = Value("SOURCEANDSINK")
  // Undefined flow direction
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
