package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 *  This enumeration defines the basic configuration of the ramp type in terms of the number
 *  and shape of ramp flights. The type also distinguished turns by landings. In addition
 *  the subdivision of the straight and changing direction ramps is included. The ramp
 *  configurations are given for ramps without and with one and two landings.
 *
 *  Ramps which are subdivided into more than two landings have to be defined by the geometry
 *  only. Also ramps with non-regular shapes have to be defined by the geometry only. The type
 *  of such ramps is USERDEFINED.
 *
 *  @since IFC Release 2.0
 */
object IfcRampTypeEnum extends IfcEnumeration{
  type IfcRampTypeEnum = Value

  // A ramp - which is a sloping floor, walk, or roadway - connecting two levels.
  // The straight ramp consists of one straight flight without turns or winders.
  val STRAIGHT_RUN_RAMP: Value = Value("STRAIGHT_RUN_RAMP")
  // A straight ramp consisting of two straight flights without turns but with one landing.
  val TWO_STRAIGHT_RUN_RAMP: Value = Value("TWO_STRAIGHT_RUN_RAMP")
  // 	A ramp making a 90° turn, consisting of two straight flights connected by a quarterspace
  // 	landing. The direction of the turn is determined by the walking line.
  val QUARTER_TURN_RAMP: Value = Value("QUARTER_TURN_RAMP")
  // A ramp making a 180° turn, consisting of three straight flights connected by two quarterspace
  // landings. The direction of the turn is determined by the walking line.
  val TWO_QUARTER_TURN_RAMP: Value = Value("TWO_QUARTER_TURN_RAMP")
  // A ramp making a 180° turn, consisting of two straight flights connected by a halfspace landing.
  // The orientation of the turn is determined by the walking line.
  val HALF_TURN_RAMP: Value = Value("HALF_TURN_RAMP")
  // A ramp constructed around a circular or elliptical well without newels and landings.
  val SPIRAL_RAMP: Value = Value("SPIRAL_RAMP")
  // Free form ramp (user defined operation type)
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
