package org.sikong.ifc.ifc2x3.interoperability.facilities.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * An IfcServiceLifeFactorTypeEnum is an enumerated list of the types of service
 * life factor that can be applied and that modify the extent of the service life.
 *
 * @since IFC 2x2
 */
object IfcServiceLifeFactorTypeEnum extends IfcEnumeration {
  type IfcServiceLifeFactorTypeEnum = Value

  // Adjustment of the service life resulting from the effect of the quality of
  // components used.
  val A_QUALITYOFCOMPONENTS: Value = Value("A_QUALITYOFCOMPONENTS")
  // Adjustment of the service life resulting from the effect of design level employed.
  val B_DESIGNLEVEL: Value = Value("B_DESIGNLEVEL")
  // Adjustment of the service life resulting from the effect of the quality of work
  // executed.
  val C_WORKEXECUTIONLEVEL: Value = Value("C_WORKEXECUTIONLEVEL")
  // Adjustment of the service life resulting from the effect of the indoor environment
  // (where appropriate).
  val D_INDOORENVIRONMENT: Value = Value("D_INDOORENVIRONMENT")
  // 	Adjustment of the service life resulting from the effect of the outdoor environment
  // 	(where appropriate)
  val E_OUTDOORENVIRONMENT: Value = Value("E_OUTDOORENVIRONMENT")
  // Adjustment of the service life resulting from the effect of the conditions in which
  // components are operating.
  val F_INUSECONDITIONS: Value = Value("F_INUSECONDITIONS")
  // Adjustment of the service life resulting from the effect of the level or degree of
  // maintenance applied to dcomponents.
  val G_MAINTENANCELEVEL: Value = Value("G_MAINTENANCELEVEL")
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
