package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the basic configuration of the window type in terms of the
 * number of window panels and the subdivision of the total window. The window configurations
 * are given for windows with one, two or three panels (including fixed panels).
 *
 * Windows which are subdivided into more than three panels have to be defined by the geometry
 * only. The type of such windows is USERDEFINED.
 *
 * @since IFC Release 2.0
 */
object IfcWindowStyleOperationEnum extends IfcEnumeration {
  type IfcWindowStyleOperationEnum = Value

  // Window with one panel.
  val SINGLE_PANEL: Value = Value("SINGLE_PANEL")
  // Window with two panels. The configuration of the panels is vertically.
  val DOUBLE_PANEL_VERTICAL: Value = Value("DOUBLE_PANEL_VERTICAL")
  // 	Window with two panels. The configuration of the panels is horizontally.
  val DOUBLE_PANEL_HORIZONTAL: Value = Value("DOUBLE_PANEL_HORIZONTAL")
  // Window with three panels. The configuration of the panels is vertically.
  val TRIPLE_PANEL_VERTICAL: Value = Value("TRIPLE_PANEL_VERTICAL")
  // 	Window with three panels. The configuration of the panels is horizontally.
  val TRIPLE_PANEL_BOTTOM: Value = Value("TRIPLE_PANEL_BOTTOM")
  // Window with three panels. The configuration of two panels is vertically and
  // the third one is horizontally at the bottom.
  val TRIPLE_PANEL_TOP: Value = Value("TRIPLE_PANEL_TOP")
  // Window with three panels. The configuration of two panels is vertically and
  // the third one is horizontally at the top.
  val TRIPLE_PANEL_LEFT: Value = Value("TRIPLE_PANEL_LEFT")
  // Window with three panels. The configuration of two panels is horizontally
  // and the third one is vertically at the left hand side.
  val TRIPLE_PANEL_RIGHT: Value = Value("TRIPLE_PANEL_RIGHT")
  // Window with three panels. The configuration of two panels is horizontally
  // and the third one is vertically at the right hand side.
  val TRIPLE_PANEL_HORIZONTAL: Value = Value("TRIPLE_PANEL_HORIZONTAL")
  // user defined operation type
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
