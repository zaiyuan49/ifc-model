package org.sikong.ifc.ifc2x3.interoperability.management.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * An IfcProjectOrderRecordTypeEnum is a designation of the type of event being recorded.
 *
 * @since IFC 2x2
 */
object IfcProjectOrderRecordTypeEnum extends IfcEnumeration {
  type IfcProjectOrderRecordTypeEnum = Value

  // A record of instructions to bring about a change to a construction or installation.
  val CHANGE: Value = Value("CHANGE")
  // A record of instructions to carry out maintenance work on one or more assets or components.
  val MAINTENANCE: Value = Value("MAINTENANCE")
  // A record of instructions to move actors and/or artefacts.
  val MOVE: Value = Value("MOVE")
  // A record of instructions to purchase goods or services.
  val PURCHASE: Value = Value("PURCHASE")
  // A record of instructions to carry out work generally.
  val WORK: Value = Value("WORK")
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
