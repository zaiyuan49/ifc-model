package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the different types of linear elements an IfcBeamType
 * object can fulfill:
 *    BEAM, JOIST, LINTEL, T_BEAM, USERDEFINED, NOTDEFINED
 * @since Release IFC2x Edition 2.
 */
object IfcBeamTypeEnum extends IfcEnumeration {
  type IfcBeamTypeEnum = Value

  // A standard beam usually used horizontally.
  val BEAM: Value = Value("BEAM")
  // A beam used to support a floor or ceiling.
  val JOIST: Value = Value("JOIST")
  // A beam or horizontal piece of material over an opening (e.g. door, window).
  val LINTEL: Value = Value("LINTEL")
  // A T-shape beam that forms part of a slab construction.
  val T_BEAM: Value = Value("T_BEAM")
  // User-defined linear beam element.
  val USERDEFINED: Value = Value("USERDEFINED")
  // Undefined linear beam element
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
