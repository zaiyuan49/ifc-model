package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

object IfcRoofTypeEnum extends IfcEnumeration {
  type IfcRoofTypeEnum = Value

  // A roof having no slope, or one with only a slight pitch so as to drain rainwater.
  val FLAT_ROOF: Value = Value("FLAT_ROOF")
  // A roof having a single slope.
  val SHED_ROOF: Value = Value("SHED_ROOF")
  // 	A roof sloping downward in two parts from a central ridge, so as to form a gable at each end.
  val GABLE_ROOF: Value = Value("GABLE_ROOF")
  // 	A roof having sloping ends and sides meeting at an inclined projecting angle.
  val HIP_ROOF: Value = Value("HIP_ROOF")
  // A roof having a hipped end truncating a gable.
  val HIPPED_GABLE_ROOF: Value = Value("HIPPED_GABLE_ROOF")
  // A ridged roof divided on each side into a shallower slope above a steeper one.
  val GAMBREL_ROOF: Value = Value("GAMBREL_ROOF")
  // A roof having on each side a steeper lower part and a shallower upper part.
  val MANSARD_ROOF: Value = Value("MANSARD_ROOF")
  // 	A roof or ceiling having a semicylindrical form.
  val BARREL_ROOF: Value = Value("BARREL_ROOF")
  // A gable roof in the form of a broad Gothic arch, with gently sloping convex surfaces.
  val RAINBOW_ROOF: Value = Value("RAINBOW_ROOF")
  // 	A roof having two slopes, each descending inward from the eaves.
  val BUTTERFLY_ROOF: Value = Value("BUTTERFLY_ROOF")
  // 	A pyramidal hip roof.
  val PAVILION_ROOF: Value = Value("PAVILION_ROOF")
  // A hemispherical hip roof.
  val DOME_ROOF: Value = Value("DOME_ROOF")
  // Free form roof
  val FREEFORM: Value = Value("FREEFORM")
  // 	No specification given
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
