package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the basic configuration of the window type in terms of the location
 * of window panels. The window configurations are given for windows with one, two or three
 * panels (including fixed panels). It corresponds to the OperationType of the IfcWindowStyle
 * definition, which references the IfcWindowPanelProperties.
 *
 * Windows which are subdivided into more than three panels have to be defined by the geometry
 * only. The type of such windows is given by an IfcWindowStyle.OperationType = USERDEFINED or
 * NOTDEFINED (see IfcWindowStyleOperationEnum for details).
 *
 * @since IFC Release 2.0
 */
object IfcWindowPanelPositionEnum extends IfcEnumeration {
  type IfcWindowPanelPositionEnum = Value

  val LEFT: Value = Value("LEFT")
  val MIDDLE: Value = Value("MIDDLE")
  val RIGHT: Value = Value("RIGHT")
  val BOTTOM: Value = Value("BOTTOM")
  val TOP: Value = Value("TOP")
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
