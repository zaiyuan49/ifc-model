package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the basic ways to describe the location of a door
 * panel within a door lining.
 * @since IFC Release 2.x
 */
object IfcDoorPanelPositionEnum extends IfcEnumeration {
  type IfcDoorPanelPositionEnum = Value

  val LEFT: Value = Value("LEFT")
  val MIDDLE: Value = Value("MIDDLE")
  val RIGHT: Value = Value("RIGHT")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
