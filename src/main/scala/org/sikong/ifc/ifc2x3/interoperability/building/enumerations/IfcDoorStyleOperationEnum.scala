package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the basic ways to describe how doors operate.
 * @since Release IFC2x
 */
object IfcDoorStyleOperationEnum extends IfcEnumeration {
  type IfcDoorStyleOperationEnum = Value

  // Door with one panel that opens (swings) to the left. The hinges are on
  // the left side as viewed in the direction of the positive y-axis.
  val SINGLE_SWING_LEFT: Value = Value("SINGLE_SWING_LEFT")
  // Door with one panel that opens (swings) to the right. The hinges are on
  // the right side as viewed in the direction of the positive y-axis.
  val SINGLE_SWING_RIGHT: Value = Value("SINGLE_SWING_RIGHT")
  // Door with two panels, one opens (swings) to the left the other opens (swings) to the right.
  val DOUBLE_DOOR_SINGLE_SWING: Value = Value("DOUBLE_DOOR_SINGLE_SWING")
  // Door with one panel that swings in both directions and to the left in the main trafic
  // direction. Also called double acting door.
  val DOUBLE_DOOR_SINGLE_SWING_OPPOSITE_LEFT: Value =
    Value("DOUBLE_DOOR_SINGLE_SWING_OPPOSITE_LEFT")
  // Door with one panel that swings in both directions and to the right in the main trafic
  // direction. Also called double acting door.
  val DOUBLE_DOOR_SINGLE_SWING_OPPOSITE_RIGHT: Value =
    Value("DOUBLE_DOOR_SINGLE_SWING_OPPOSITE_RIGHT")
  // Door with two panels, one swings in both directions and to the right in the main trafic
  // direction the other swings also in both directions and to the left in the main trafic
  // direction.
  val DOUBLE_SWING_LEFT: Value = Value("DOUBLE_SWING_LEFT")
  // Door with two panels that both open to the left, one panel swings in one direction and
  // the other panel swings in the opposite direction.
  val DOUBLE_SWING_RIGHT: Value = Value("DOUBLE_SWING_RIGHT")
  // Door with two panels that both open to the right, one panel swings in one direction and
  // the other panel swings in the opposite direction.
  val DOUBLE_DOOR_DOUBLE_SWING: Value = Value("DOUBLE_DOOR_DOUBLE_SWING")
  // Door with one panel that is sliding to the left.
  val SLIDING_TO_LEFT: Value = Value("SLIDING_TO_LEFT")
  // Door with one panel that is sliding to the right.
  val SLIDING_TO_RIGHT: Value = Value("SLIDING_TO_RIGHT")
  // Door with two panels, one is sliding to the left the other is sliding to the right.
  val DOUBLE_DOOR_SLIDING: Value = Value("DOUBLE_DOOR_SLIDING")
  // 	Door with one panel that is folding to the left.
  val FOLDING_TO_LEFT: Value = Value("FOLDING_TO_LEFT")
  // 	Door with one panel that is folding to the right.
  val FOLDING_TO_RIGHT: Value = Value("FOLDING_TO_RIGHT")
  // 	Door with two panels, one is folding to the left the other is folding to the right.
  val DOUBLE_DOOR_FOLDING: Value = Value("DOUBLE_DOOR_FOLDING")
  // An entrance door consisting of four leaves set in a form of a cross and revolving
  // around a central vertical axis (the four panels are described by a single IfcDoor
  // panel property).
  val REVOLVING: Value = Value("REVOLVING")
  // 	Door that opens by rolling up.
  val ROLLINGUP: Value = Value("ROLLINGUP")
  // 	User defined operation type
  val USERDEFINED: Value = Value("USERDEFINED")
  // 	A door with a not defined operation type is considered as a door with a lining,
  // 	but no panels. It is thereby always open.
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
