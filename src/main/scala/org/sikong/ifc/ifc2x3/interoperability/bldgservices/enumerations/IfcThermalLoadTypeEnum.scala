package org.sikong.ifc.ifc2x3.interoperability.bldgservices.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the type of thermal load for spaces or zones, as derived
 * from various use cases
 *
 * @since IFC R2x2
 */
object IfcThermalLoadTypeEnum extends IfcEnumeration {
  type IfcThermalLoadTypeEnum = Value

  // Energy added or removed from air that affects its temperature.
  val SENSIBLE: Value = Value("SENSIBLE")
  // Energy added or removed from air that affects its humidity or concentration of water
  // vapor.
  val LATENT: Value = Value("LATENT")
  // Electromagnetic energy added or removed by emmission or absorption.
  val RADIANT: Value = Value("RADIANT")
  // Undefined thermal load type.
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
