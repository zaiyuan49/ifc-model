package org.sikong.ifc.ifc2x3.interoperability.facilities.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * IfcInventoryTypeEnum defines the types of inventory that can be defined.
 *
 * @since IFC Release 2.0
 */
object IfcInventoryTypeEnum extends IfcEnumeration {
  type IfcInventoryTypeEnum = Value

  // A list of asset instances of type IfcAsset
  val ASSETINVENTORY: Value = Value("ASSETINVENTORY")
  // A list of space instances of type IfcSpace
  val SPACEINVENTORY: Value = Value("SPACEINVENTORY")
  // A list of furniture instances of type IfcFurniture
  val FURNITUREINVENTORY: Value = Value("FURNITUREINVENTORY")
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
