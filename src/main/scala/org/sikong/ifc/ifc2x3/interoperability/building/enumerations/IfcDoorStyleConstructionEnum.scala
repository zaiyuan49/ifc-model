package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the basic types of construction of doors. The construction
 * type relates to the main material (or material combination) used for making the door.
 * @since IFC Release 2x
 */
object IfcDoorStyleConstructionEnum extends IfcEnumeration {
  type IfcDoorStyleConstructionEnum = Value

  val ALUMINIUM: Value = Value("ALUMINIUM")
  val HIGH_GRADE_STEEL: Value = Value("HIGH_GRADE_STEEL")
  val STEEL: Value = Value("STEEL")
  val WOOD: Value = Value("WOOD")
  val ALUMINIUM_WOOD: Value = Value("ALUMINIUM_WOOD")
  val ALUMINIUM_PLASTIC: Value = Value("ALUMINIUM_PLASTIC")
  val PLASTIC: Value = Value("PLASTIC")
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
