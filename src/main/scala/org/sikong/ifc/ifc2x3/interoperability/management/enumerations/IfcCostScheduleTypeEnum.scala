package org.sikong.ifc.ifc2x3.interoperability.management.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * An IfcCostScheduleTypeEnum is a list of the available types of cost schedule from
 * which that required may be selected.
 *
 * @since IFC 2x2
 */
object IfcCostScheduleTypeEnum extends IfcEnumeration {
  type IfcCostScheduleTypeEnum = Value

  // An allocation of money for a particular purpose.
  val BUDGET: Value = Value("BUDGET")
  // An assessment of the amount of money needing to be expended for a defined purpose
  // based on incomplete information about the goods and services required for a
  // construction or installation.
  val COSTPLAN: Value = Value("COSTPLAN")
  // An assessment of the amount of money needing to be expended for a defined purpose
  // based on actual information about the goods and services required for a construction
  // or installation.
  val ESTIMATE: Value = Value("ESTIMATE")
  // An offer to provide goods and services.
  val TENDER: Value = Value("TENDER")
  // A complete listing of all work items forming construction or installation works in
  // which costs have been allocated to work items.
  val PRICEDBILLOFQUANTITIES: Value = Value("PRICEDBILLOFQUANTITIES")
  // 	A complete listing of all work items forming construction or installation works in
  // 	which costs have not yet been allocated to work items.
  val UNPRICEDBILLOFQUANTITIES: Value = Value("UNPRICEDBILLOFQUANTITIES")
  // A listing of each type of goods forming construction or installation works with the
  // cost of purchase, construction/installation, overheads and profit assigned so that
  // additional items of that type can be costed.
  val SCHEDULEOFRATES: Value = Value("SCHEDULEOFRATES")
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
