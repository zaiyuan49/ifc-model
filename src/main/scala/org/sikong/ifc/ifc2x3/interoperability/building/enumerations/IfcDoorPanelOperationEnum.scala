package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the basic ways how individual door panels operate.
 * @since IFC Release 2.0
 */
object IfcDoorPanelOperationEnum extends IfcEnumeration {
  type IfcDoorPanelOperationEnum = Value

  val SWINGING: Value = Value("SWINGING")
  val DOUBLE_ACTING: Value = Value("DOUBLE_ACTING")
  val SLIDING: Value = Value("SLIDING")
  val FOLDING: Value = Value("FOLDING")
  val REVOLVING: Value = Value("REVOLVING")
  val ROLLINGUP: Value = Value("ROLLINGUP")
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
