package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the different types of walls
 * @since Release IFC2x Edition 2
 */
object IfcWallTypeEnum extends IfcEnumeration {
  type IfcWallTypeEnum = Value

  // A standard wall, extruded vertically with a constant thickness along the wall path.
  val STANDARD: Value = Value("STANDARD")
  // A polygonal wall, extruded vertically, where the wall thickness changes along the wall path.
  val POLYGONAL: Value = Value("POLYGONAL")
  // A shear wall, having a non-rectangular cross section.
  val SHEAR: Value = Value("SHEAR")
  // User-defined wall element.
  val USERDEFINED: Value = Value("USERDEFINED")
  // Undefined wall element
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
