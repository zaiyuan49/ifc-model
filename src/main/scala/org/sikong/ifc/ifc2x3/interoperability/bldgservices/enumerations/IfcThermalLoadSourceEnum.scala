package org.sikong.ifc.ifc2x3.interoperability.bldgservices.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the various sources of thermal gains or losses for
 * spaces or zones, derived from various use cases
 *
 * @since IFC R2.0
 */
object IfcThermalLoadSourceEnum extends IfcEnumeration {
  type IfcThermalLoadSourceEnum = Value

  // Heat gains and losses from people.
  val PEOPLE: Value = Value("PEOPLE")
  // Lighting loads.
  val LIGHTING: Value = Value("LIGHTING")
  // Heat gains and losses from equipment.
  val EQUIPMENTSENSIBLE: Value = Value("EQUIPMENTSENSIBLE")
  // Ventilation loads from indoor air.
  val VENTILATIONINDOORAIR: Value = Value("VENTILATIONINDOORAIR")
  // Ventilation loads from outside air.
  val VENTILATIONOUTSIDEAIR: Value = Value("VENTILATIONOUTSIDEAIR")
  // Loads from recirculated air.
  val RECIRCULATEDAIR: Value = Value("RECIRCULATEDAIR")
  // Loads from exhaust air.
  val EXHAUSTAIR: Value = Value("EXHAUSTAIR")
  // Loads from the air exchange rate.
  val AIREXCHANGERATE: Value = Value("AIREXCHANGERATE")
  // Loads from the dry bulb temperature.
  val DRYBULBTEMPERATURE: Value = Value("DRYBULBTEMPERATURE")
  // Loads from the relative humidity.
  val RELATIVEHUMIDITY: Value = Value("RELATIVEHUMIDITY")
  // Heat gains and losses from infiltration.
  val INFILTRATIONSENSIBLE: Value = Value("INFILTRATIONSENSIBLE")
  // User-defined thermal load type.
  val USERDEFINED: Value = Value("USERDEFINED")
  // Undefined thermal load type.
  val NOTDEFINED: Value = Value("NOTDEFINED")

}
