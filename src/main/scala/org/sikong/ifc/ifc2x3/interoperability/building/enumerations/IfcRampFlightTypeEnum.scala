package org.sikong.ifc.ifc2x3.interoperability.building.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration defines the different types of linear elements
 * @since Release IFC2x Edition 2
 */
object IfcRampFlightTypeEnum extends IfcEnumeration {
  type IfcRampFlightTypeEnum = Value

  // A ramp flight with a straight walking line.
  val STRAIGHT: Value = Value("STRAIGHT")
  // A ramp flight with a circular or elliptic walking line.
  val SPIRAL: Value = Value("SPIRAL")
  // User-defined ramp flight.
  val USERDEFINED: Value = Value("USERDEFINED")
  // Undefined ramp flight.
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
