package org.sikong.ifc.ifc2x3.interoperability.bldgservices.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * This enumeration is used to identify the sequence of usage of the energy source.
 *
 * @since IFC 2x2
 */
object IfcEnergySequenceEnum extends IfcEnumeration {
  type IfcEnergySequenceEnum = Value

  // Primary energy source.
  val PRIMARY: Value = Value("PRIMARY")
  // Secondary energy source.
  val SECONDARY: Value = Value("SECONDARY")
  // Tertiary energy source.
  val TERTIARY: Value = Value("TERTIARY")
  // Auxiliary.
  val AUXILIARY: Value = Value("AUXILIARY")
  // User-defined energy sequence.
  val USERDEFINED: Value = Value("USERDEFINED")
  // Undefined energy sequence.
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
