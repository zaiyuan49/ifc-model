package org.sikong.ifc.ifc2x3.interoperability.facilities.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * An IfcServiceLifeTypeEnum is an enumerated list of the types of service life
 * of an artefact
 *
 * @since IFC 2x2
 */
object IfcServiceLifeTypeEnum extends IfcEnumeration {
  type IfcServiceLifeTypeEnum = Value

  // The service life that an asset has given.
  val ACTUALSERVICELIFE: Value = Value("ACTUALSERVICELIFE")
  // The service life that an artefact is expected to have under current operating conditions.
  val EXPECTEDSERVICELIFE: Value = Value("EXPECTEDSERVICELIFE")
  // The best or most optimistic estimate of service life that is quoted for an artefact under
  // reference operating conditions.
  val OPTIMISTICREFERENCESERVICELIFE: Value = Value("OPTIMISTICREFERENCESERVICELIFE")
  // The least or most pessimistic estimate of service life that is quoted for an artefact
  // under reference operating conditions.
  val PESSIMISTICREFERENCESERVICELIFE: Value = Value("PESSIMISTICREFERENCESERVICELIFE")
  // The typical service life that is quoted for an artefact under reference operating conditions.
  val REFERENCESERVICELIFE: Value = Value("REFERENCESERVICELIFE")
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
