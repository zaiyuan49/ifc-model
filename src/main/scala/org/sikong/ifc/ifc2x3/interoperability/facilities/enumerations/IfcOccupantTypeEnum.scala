package org.sikong.ifc.ifc2x3.interoperability.facilities.enumerations

import org.sikong.ifc.ifc2x3.base.IfcEnumeration

/**
 * IfcOccupantTypeEnum defines the types of occupant from which the type required
 * can be selected.
 *
 * @since IFC Release 2.0
 */
object IfcOccupantTypeEnum extends IfcEnumeration {
  type IfcOccupantTypeEnum = Value

  // Actor receiving the assignment of a property agreement from an assignor
  val ASSIGNEE: Value = Value("ASSIGNEE")
  // Actor assigning a property agreement to an assignor
  val ASSIGNOR: Value = Value("ASSIGNOR")
  // Actor receiving the lease of a property from a lessor
  val LESSEE: Value = Value("LESSEE")
  // Actor leasing a property to a lessee
  val LESSOR: Value = Value("LESSOR")
  // Actor participating in a property agreement on behalf of an owner, lessor
  // or assignor
  val LETTINGAGENT: Value = Value("LETTINGAGENT")
  // Actor that owns a property
  val OWNER: Value = Value("OWNER")
  // Actor renting the use of a property fro a period of time
  val TENANT: Value = Value("TENANT")
  val USERDEFINED: Value = Value("USERDEFINED")
  val NOTDEFINED: Value = Value("NOTDEFINED")
}
