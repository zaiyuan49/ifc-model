package org.sikong.ifc.ifc2x3.base

import org.sikong.ifc.ifc2x3.base.frame.IfcBasicNode

class IfcString(val value: String) extends IfcBasicNode(value) {
  override def getStepParameter(isSelectType: Boolean): String = ???

  override def getStepLine: String = ???

  override def getClassName: String = "STRING"
}

object IfcString {

  def apply(): IfcString = new IfcString(null)

  def apply(value: String): IfcString = new IfcString(value)

  def apply(value: IfcString): IfcString = new IfcString(value.value)

}
