package org.sikong.ifc.ifc2x3.base

import org.sikong.ifc.ifc2x3.base.frame.IfcBasicNode

class IfcLogical(val value: Boolean) extends IfcBasicNode(value) {

  override def getStepParameter(isSelectType: Boolean): String = ???

  override def getStepLine: String = ???

  override def getClassName: String = "BOOLEAN"
}

object IfcLogical {

  def apply(value: IfcLogical): IfcLogical = new IfcLogical(value.value)

  def apply(value: Boolean): IfcLogical = new IfcLogical(value)

  def apply(value: String): IfcLogical = value.toUpperCase match {
    case "T" => new IfcLogical(true)
    case "F" => new IfcLogical(false)
    // TODO 以后这里要解决
    case "U" => throw new UnsupportedOperationException
  }

}
