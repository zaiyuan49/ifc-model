package org.sikong.ifc.ifc2x3.base.frame

trait IfcNode {

  def getStepParameter(isSelectType: Boolean): String

  def getStepLine: String

  def getClassName: String = this.getClass.getSimpleName

}
