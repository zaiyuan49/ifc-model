package org.sikong.ifc.ifc2x3.base

import org.sikong.ifc.ifc2x3.base.frame.IfcBasicNode

class IfcDouble(val value: Double) extends IfcBasicNode(value) {
  override def getStepParameter(isSelectType: Boolean): String = ???

  override def getStepLine: String = ???

  override def getClassName: String = "DOUBLE"
}

object IfcDouble {

  def apply(): IfcDouble = new IfcDouble(0.0D)

  def apply(value: Double): IfcDouble = new IfcDouble(value)

  def apply(value: IfcDouble): IfcDouble = new IfcDouble(value.value)

  def apply(value: Number): IfcDouble = new IfcDouble(value.doubleValue())

}
