package org.sikong.ifc.ifc2x3.base

import org.sikong.ifc.ifc2x3.base.frame.IfcBasicNode

class IfcBinary(val value: Int = 0) extends IfcBasicNode(value) {
  override def getStepParameter(isSelectType: Boolean): String = ???

  override def getStepLine: String = ???

  override def getClassName: String = "BINARY"
}

object IfcBinary {

  def apply(): IfcBinary = new IfcBinary()

  def apply(value: IfcBinary): IfcBinary = new IfcBinary(value.value)

  def apply(value: Int): IfcBinary = new IfcBinary(value)

  def apply(value: String): IfcBinary = new IfcBinary(value.toInt)

}
