package org.sikong.ifc.ifc2x3.base

import org.sikong.ifc.ifc2x3.base.frame.IfcBasicNode

class IfcBoolean(val value: Boolean) extends IfcBasicNode(value) {

  override def getStepParameter(isSelectType: Boolean): String =
    if (value) ".T." else ".F."

  override def getStepLine: String = null

  override def getClassName: String = "BOOLEAN"
}

object IfcBoolean {

  def apply(): IfcBoolean = new IfcBoolean(false)

  def apply(value: Boolean): IfcBoolean = new IfcBoolean(value)

  def apply(value: IfcBoolean): IfcBoolean = new IfcBoolean(value.value)

}
