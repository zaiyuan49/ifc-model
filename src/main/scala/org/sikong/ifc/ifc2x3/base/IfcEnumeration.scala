package org.sikong.ifc.ifc2x3.base

class IfcEnumeration extends Enumeration {

  def apply(value: String): Value = this.withName(value)

  def getClassName: String = this.getClass.getSimpleName

  def getStepParameter(value: String, isSelectType: Boolean): String = {
    val itemStr = s".${this(value)}."
    if (isSelectType) s"${getClassName.toUpperCase}($itemStr)" else itemStr
  }
}