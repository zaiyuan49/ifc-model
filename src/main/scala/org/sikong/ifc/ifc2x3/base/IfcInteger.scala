package org.sikong.ifc.ifc2x3.base

import org.sikong.ifc.ifc2x3.base.frame.IfcBasicNode

class IfcInteger(val value: Int) extends IfcBasicNode(value) {
  override def getStepParameter(isSelectType: Boolean): String = ???

  override def getStepLine: String = ???

  override def getClassName: String = "INTEGER"
}

object IfcInteger {

  def apply(): IfcInteger = new IfcInteger(0)

  def apply(value: Int): IfcInteger = new IfcInteger(value)

  def apply(value: IfcInteger): IfcInteger = new IfcInteger(value.value)

}
