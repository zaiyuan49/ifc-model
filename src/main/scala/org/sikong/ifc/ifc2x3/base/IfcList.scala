package org.sikong.ifc.ifc2x3.base

import org.sikong.ifc.ifc2x3.base.frame.{IfcBasicNode, IfcNode}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.jdk.CollectionConverters._

class IfcList[E <: IfcNode](val value: mutable.Buffer[E])
  extends IfcBasicNode(value) {

  override def getStepParameter(isSelectType: Boolean): String = ???

  override def getStepLine: String = ???

  override def getClassName: String = "LIST"
}

object IfcList {

  def apply[E <: IfcNode](): IfcList[E] = new IfcList(new ListBuffer[E])

  def apply[E <: IfcNode](value: mutable.Buffer[E]): IfcList[E] = new IfcList(value)

  def apply[E <: IfcNode](value: java.util.ArrayList[E]): IfcList[E] = new IfcList(value.asScala)

}
