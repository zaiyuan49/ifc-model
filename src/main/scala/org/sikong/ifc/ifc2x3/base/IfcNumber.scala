package org.sikong.ifc.ifc2x3.base

import org.sikong.ifc.ifc2x3.base.frame.IfcBasicNode

class IfcNumber(val value: Double) extends IfcBasicNode(value) {

  override def getStepParameter(isSelectType: Boolean): String = ???

  override def getStepLine: String = ???

  override def getClassName: String = "NUMBER"
}

object IfcNumber {

  def apply(): IfcNumber = new IfcNumber(0.0D)

  def apply(value: Double): IfcNumber = new IfcNumber(value)

  def apply(value: Int): IfcNumber = new IfcNumber(value)

  def apply(value: IfcNumber): IfcNumber = new IfcNumber(value.value)

  def apply(value: Number): IfcNumber = new IfcNumber(value.doubleValue())

}
