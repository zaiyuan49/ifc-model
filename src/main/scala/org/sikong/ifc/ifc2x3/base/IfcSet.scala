package org.sikong.ifc.ifc2x3.base

import org.sikong.ifc.ifc2x3.base.frame.{IfcBasicNode, IfcNode}

import java.util
import scala.collection.mutable
import scala.jdk.CollectionConverters._

class IfcSet[E <: IfcNode](val value: mutable.Set[E]) extends IfcBasicNode(value) {

  override def getStepParameter(isSelectType: Boolean): String = ???

  override def getStepLine: String = ???

  override def getClassName: String = "SET"

}

object IfcSet {

  def apply[E <: IfcNode](): IfcSet[E] = new IfcSet(new mutable.HashSet[E])

  def apply[E <: IfcNode](value: mutable.Set[E]): IfcSet[E] = new IfcSet(value)

  def apply[E <: IfcNode](value: util.Set[E]): IfcSet[E] = new IfcSet(value.asScala)

}
